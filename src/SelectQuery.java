import java.sql.DriverManager;
import java.sql.SQLException;

public class SelectQuery {
	
	// this file was used to test the created database to ensure values were read in properly
	
	public static void main(String args[]) throws SQLException {
		final String CONNECTION_STRING = "jdbc:derby:Databases/MovieStats";
		
		DriverManager.registerDriver(new org.apache.derby.jdbc.EmbeddedDriver());
		
		var connection = DriverManager.getConnection(CONNECTION_STRING);
		var statement = connection.createStatement();
		var result = statement.executeQuery("SELECT * FROM WeeklyUpdate WHERE TicketsSold > 1000");
		
		System.out.println("Movies with tickets sold over 1000");
		System.out.println("----------------------------------");
		
		while (result.next()) {
			System.out.println(result.getString("MovieName") + " (" + result.getString("TicketsSold") + ")");
		}
		
		statement.close();
		connection.close();
	}
	
}
