import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ProcessFile {
	
	public static void main(String args[]) throws IOException, SQLException {
		final String CONNECTION_STRING = "jdbc:derby:Databases/MovieStats";
		
		DriverManager.registerDriver(new org.apache.derby.jdbc.EmbeddedDriver());
		
		var connection = DriverManager.getConnection(CONNECTION_STRING);
		var statement = connection.createStatement();
		var file = new File("Weekly-Movie-Stats.txt");
		var br = new BufferedReader(new FileReader(file));
		var line = "";
		var splitBy = ", ";
		
		while ((line = br.readLine()) != null) {
			var movies = line.split(splitBy);
			
			var exists = statement.executeQuery("SELECT * FROM WeeklyUpdate WHERE MovieName LIKE '%" + movies[0] + "%'");
			if (exists.next()) {
				statement.executeUpdate("UPDATE WeeklyUpdate SET "
						+ "Showings = " + movies[1] + ", "
						+ "Screens = " + movies[2] + ", "
						+ "TicketsSold = " + movies[3] + ", "
						+ "Receipts = " + movies[4] + " "
						+ "WHERE MovieName = '" + exists.getString("MovieName") + "'");
			} else {
				statement.executeUpdate("INSERT INTO WeeklyUpdate VALUES"
						+ "('" + movies[0] + "',"
						+ movies[1] + ","
						+ movies[2] + ","
						+ movies[3] + ","
						+ movies[4] + ")");
			}
		}
		
		br.close();
		statement.close();
		connection.close();
	}

}
