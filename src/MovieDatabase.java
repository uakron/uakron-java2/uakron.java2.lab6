import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class MovieDatabase {
	
	public static void main(String args[]) throws SQLException {
		final String CONNECTION_STRING = "jdbc:derby:Databases/MovieStats;create=true";
		
		DriverManager.registerDriver(new org.apache.derby.jdbc.EmbeddedDriver());
		
		Connection connection = DriverManager.getConnection(CONNECTION_STRING);
		
		Statement statement = connection.createStatement();
		
		try {
			statement.execute("CREATE TABLE WeeklyUpdate ("
					+ "MovieName VARCHAR(255) NOT NULL PRIMARY KEY, "
					+ "Showings INT, "
					+ "Screens INT, "
					+ "TicketsSold INT, "
					+ "Receipts INT)");
		} catch (SQLException e) {
			System.out.println("Table 'WeeklyUpdate' already exists in database!");
		}
		
		
		statement.close();
		connection.close();
	}
	
}
